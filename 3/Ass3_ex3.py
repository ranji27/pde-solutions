import numpy as np

def polymultiply(A,B):
    #Function to multiply two given polynomials
    #A and B are list of coefficients of the two polynomials
    #Polynomials to be expressed in the form a0 + a1*x^1 + a2*x^2 + .... + an*x^n
    
    c=len(A)+len(B)-1 #Calculate the length of the resulting polynomial
   
    while (c & (c-1) !=0): #Make the value of length a power of 2
        c+=1    
    
    for i in range(len(A),c): #Append zeros to increase the length to 'c'
        A.append(0)
    
    for i in range(len(B),c):
        B.append(0)
        
    A_f=np.fft.fft(A) #FFT on list A
    B_f=np.fft.fft(B) #FFT on list A
    
    C_f=np.multiply(A_f,B_f) #Elementwise multiplication of the samples
    
    C_temp=np.fft.ifft(C_f) #Inverse FFT to get the co-efficients (they are not yet divided by the number of samples)
    print('FFT multiplication',C_temp)

A=[2,3,5] #co-efficients of first polynomial
B=[-1,-2,2] #co-efficients of second polynomial
polymultiply(A,B)
print('Direct multiplication: ',np.polynomial.polynomial.polymul(A,B))


    
