import sympy as sym    
import numpy as np
x1, x2, x3 = sym.symbols('x1 x2 x3') #declaring the varibles as symbols
H=[] #declaring H 

f = x1**2 + 2*x2**2+(x1**2)*(x2**2)+sym.exp(x2**2+x3**2)-x1+x3 # given function
grad= sym.derive_by_array(f,(x1,x2,x3)) #Calculating the gradient 
g_f=np.reshape(grad,(3,1)) #converting 1x3 into 3x1
grad_f = sym.lambdify((x1, x2, x3), g_f)   # treating the expressions in g_f as functions

for i in range(len(grad)): #calculating Hesse 
    h=sym.derive_by_array(grad[i],(x1,x2,x3)) 
    H.append(h) 
H=np.reshape(H,(3,3)) #Hesse Matrix
hesse_f=sym.lambdify((x1, x2, x3), H) # lambda from an expression for Hesse

x=[[0],[0],[0]] #initialising at 0,0,0

delta_x=[] #initialising delta_x
norm_grad_f=1 
count=1 #count the iterations
while  norm_grad_f>=0.001:
    delta_x=np.linalg.solve(hesse_f(x[0][0],x[1][0],x[2][0]),grad_f(x[0][0],x[1][0],x[2][0])) #Compute delta_x 
    x=x-delta_x #compute new value of x
    norm_grad_f=np.linalg.norm(grad_f(x[0][0],x[1][0],x[2][0])) #compute norm
    count+=1
    
print("The given function is minimised at ")
print ('x1',x[0])
print ('x2',x[1])
print ('x3',x[2])
print("After "+str(count)+" iterations the final value of norm is ",norm_grad_f)
