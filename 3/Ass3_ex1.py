import numpy as np
import matplotlib.pyplot as plt


k=7 #calculating the kth sum

x = np.linspace(-np.pi, np.pi, 100) # x values
y = np.piecewise(x, [x == 0, x < 0, x > 0], [0, 1, lambda x:x]) #calculating corresponding y values

# Calculating fourier coefficients
a_0 = 1 + 1/2 * np.pi
a_n = lambda n: ((-1)**n-1)/(np.pi*n**2)
b_n = lambda n: 1/(np.pi*n) * (-1 + (-1)**n - (np.pi*(-1)**n))

#function calculating fourier series
def f(t, k):
	return a_0/2 + sum([a_n(n)*np.cos(n*t) + b_n(n)*np.sin(n*t) for n in range(1, k+1)])

#Plotting fourier series
plt.plot(x, y,label='Original funtion',)
plt.plot(x,[f(t, k) for t in x],'-r',label='fourier series')
plt.legend(loc='upper left')
plt.title('Fourier Series')
plt.show()
