

import numpy as np
import matplotlib.pyplot as plt

def initial_condition(domain_space):
    """
    Function to compute the initial condition for the heat equation problem
    """
    x=domain_space
    f=np.piecewise(x,[(x>0.25) & (x < 0.75),(x<0.25) | (x > 0.75)],[1,0]) #initialising values of f(x)
    return f

def transient_heat_equation(domain_space,value_of_time_step,number_of_time_steps,initial_condition):
    """
    Function to solve the transient heat equation with boundary condition (0,0) using Crank-Nicholson scheme.
    """
    x=domain_space
    n = len(x)
    
    tau=value_of_time_step
    t=number_of_time_steps

    f=initial_condition

    h=(x[-1]-x[0])/(n-1) #value of each division
    
    A=np.zeros((n-2,n-2)) #'A' matrix to store the co-efficients of the difference equations
    np.fill_diagonal(A,2/(h**2))     #Assigning values to co-efficient matrix 'A'
    np.fill_diagonal(A[1:],-1/(h**2))
    np.fill_diagonal(A[:,1:],-1/(h**2))
    temp=np.identity((n-2))
    for i in range(t):
        temp=np.matmul(temp,((np.linalg.inv(np.identity((n-2))+(tau/2)*A))@(np.identity((n-2))-(tau/2)*A)))
    

    u=np.matmul(temp,f[1:-1])   
        
    u=np.insert(u,0,0) #Inserting the value at x=0 (First Boundary Condition)
    u=np.append(u,0) #Inserting the value at last point (Second Boundary Condition)
    plt.plot(x,u,label="u after time %f using Crank-Nicholson scheme"%(t*tau)) #Plotting the final values
    plt.xlabel("x")
    plt.ylabel("u")
    plt.legend(loc='lower center')
    plt.show()

transient_heat_equation(np.linspace(0,1,41),0.0001,1000,initial_condition(np.linspace(0,1,41))) #Solving transient heat equation for tau/h^2 less than 0.5
transient_heat_equation(np.linspace(0,1,41),0.001,100,initial_condition(np.linspace(0,1,41))) #Solving transient heat equation for tau/h^2 greater than 0.5



