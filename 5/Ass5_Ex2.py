import numpy as np
import matplotlib.pyplot as plt

def initial_condition(domain_space):
    """
    Function to compute the initial condition for the heat equation problem
    """
    f=np.piecewise(x,[(x>0.25) & (x < 0.75),(x<0.25) | (x > 0.75)],[1,0]) #initialising values of f(x)
    return f

def transient_heat_equation(domain_space,value_of_time_step,number_of_time_steps,initial_condition,difference_scheme):
    """
    Function to solve the transient heat equation with boundary condition (0,0). The value of difference_scheme
    determines the type of time differencing: "forward" for forward time differencing and "backward" for backward
    time differencing.
    """
    x=domain_space
    n = len(x)
    
    tau=value_of_time_step
    t=number_of_time_steps

    f=initial_condition

    h=(x[-1]-x[0])/(n-1) #value of each division

    A=np.zeros((n-2,n-2)) #'A' matrix to store the co-efficients of the difference equations
    np.fill_diagonal(A,2/(h**2))     #Assigning values to co-efficient matrix 'A'
    np.fill_diagonal(A[1:],-1/(h**2))
    np.fill_diagonal(A[:,1:],-1/(h**2))

    if difference_scheme=="forward":
        #Solving the system in forward time difference scheme
        print("tau/h^2 is ",(tau/h**2),"\n")
        if ((tau/(h**2))>0.5):
            print("Results obtained are not stable as required criterion not met - there are some eigen values >1 or <-1\n")
        else:
            print("Results obtained are stable as required criterion met - all eigen values <1 and >-1\n")
            
        w,v=np.linalg.eig(np.identity((n-2))-tau*A )
        print("The eigen values are: ",w,"\n")
        print(max(w),min(w))
#        u=np.matmul(np.linalg.matrix_power((np.identity((n-2))-tau*A ),t),f[1:-1])   
        
        
    elif difference_scheme=="backward":
        #Solving the system in forward time difference scheme
        print("tau/h^2 is ",(tau/h**2),"\n")
        print("Results obtained are stable irrespective of the value of tau/h^2 - all eigen values are always <1 and >-1\n")
        w,v=np.linalg.eig(np.linalg.inv(np.identity((n-2))+tau*A ))
        print("The eigen values are: ",w,"\n")
        print(max(w),min(w))
        
#        u=np.linalg.solve(np.linalg.matrix_power((np.identity((n-2))+tau*A),t),f[1:-1])
        

x=np.linspace(0,1,41) #a linear space of the distance given

transient_heat_equation(np.linspace(0,1,41),0.00001,10000,initial_condition(np.linspace(0,1,41)),"forward") #Solving transient heat equation with forward time differencing
transient_heat_equation(np.linspace(0,1,41),0.0004,250,initial_condition(np.linspace(0,1,41)),"forward") #Solving transient heat equation with forward time differencing

transient_heat_equation(np.linspace(0,1,41),0.00001,10000,initial_condition(np.linspace(0,1,41)),"backward") #Solving transient heat equation with backward time differencing
transient_heat_equation(np.linspace(0,1,41),0.0004,250,initial_condition(np.linspace(0,1,41)),"backward") #Solving transient heat equation with backward time differencing





