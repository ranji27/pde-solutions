
import numpy as np
import math as m
import scipy.linalg as scilin

def inverse (A): 
    """
    Function to compute the inverse of a matrix by two ways;
    1. Using the definition of the function of a matrix
    2. Using the built-in function
    """
    
    print("Given matrix is:\n\n",A,"\n")
    
    w,v=np.linalg.eig(A) #Eigen decomposition
        
    v_inv=np.linalg.inv(v) #Inverse of the eigen vector matrix
      
    E=np.zeros((np.shape(A)),dtype=complex) #zero matrix to create the eigen value matrix
    row,col=np.diag_indices_from(E) #Obtain list of indices of the diagonal elements
    E[row,col]= [1/x for x in w ] #Store the eigen values in the diagonal (1/x is considered to compute the inverse)

    inverse_A=v@E@v_inv #Calculating the inverse matrix fromn eigen decomposition
    inverse_A=inverse_A.real
    
    print("The inverse of the given matrix computed two ways: \n")
    
    print("Result from \"definition of function of matrix\":\n\n",inverse_A,"\n")
    
    built_in_inv_function=np.linalg.inv(A) #Compute inverse using the built in function
    print("Result from built-in inverse function:\n\n",built_in_inv_function,"\n")
    
def exponent (A): 
    
    w,v=np.linalg.eig(A) #Eigen decomposition 
    
    v_inv=np.linalg.inv(v) #Inverse of the eigen vector matrix
    E=np.zeros((np.shape(A)),dtype=complex) #zero matrix to create the eigen value matrix
    row,col=np.diag_indices_from(E) #Obtain list of indices of the diagonal elements    
    E[row,col]= [m.e**(x) for x in w ]
       
    exp_A=v@E@v_inv
    exp_A=exp_A.real
        
    print("The exponent of the given matrix computed in three ways: \n")
    
    print("Result from \"definition of function of matrix\":\n\n",exp_A,"\n")
    
    e_A=np.identity(len(A[:,1]))
    fact=1
    mul=np.identity(len(A[:,1]))
    for i in range (1,20):
        fact=fact*i
        mul=mul@A
        e_A=e_A+(mul/fact)  
  
    print("Result from power series formula:\n\n",e_A,"\n")
    built_in_exp_function=(scilin.expm(A))       
    
    print("\nResult from built-in exponent function:\n\n",built_in_exp_function,"\n")
   
    
while 1:
    A=np.random.randn(10,10)
    w,v=np.linalg.eig(A) #Eigen decomposition
    
    if (len(np.unique(w))==len(w)) and (np.count_nonzero(w)==len(w) ): #checking if the given matrix is diagonalizable and invertible by ensuring all the eigen values are unique and there are no zero eigen values
        break



inverse(A) #Calling the inverse function
exponent (A) #Calling the exponent function
