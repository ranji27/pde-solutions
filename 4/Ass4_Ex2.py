
import numpy as np
import matplotlib.pyplot as plt

n = 41 #Number of points
x=np.linspace(0,1,n) #linear discretization
h=(x[-1]-x[0])/(n-1) #step length
f=np.piecewise(x,[(x>0.25) & (x < 0.75),(x<0.25) | (x > 0.75)],[1,0]) #declaration of function f(x)
A=np.zeros((n-2,n-2)) #'A' tri-diagonal matrix
#Assigning values to tri-diagonal matrix A
np.fill_diagonal(A,2/(h**2))     
np.fill_diagonal(A[1:],-1/(h**2))
np.fill_diagonal(A[:,1:],-1/(h**2))
#Solving Au = f for u
u=np.linalg.solve(A,f[1:n-1]) 
u=np.insert(u,0,0) #Inserting the value at x=0 (First Boundary Condition)
u=np.append(u,[0]) #Inserting the value at last point (Second Boundary Condition)

#plt.plot(x,f,label="Function f")
plt.plot(x,u,label="Result u") #Plotting the final values

plt.xlabel("x")
plt.ylabel("u")
plt.legend(loc='best')
plt.show()
