

import numpy as np
import matplotlib.pyplot as plt 

def first_order_diff (x,f,n,stencil,typ):
    """
        Function to calculate the first order differential.
        "x" is the variable which is taken as n points between 0 and 1
        "f" contains function values at points in variable f(x) = x^2
        "n" number of points in x which are considered for evaluation of derivative
        "stencil" should be 3 for 3-point stencil and 5 for 5-point stencil
        "typ" indicates the type of differencing to be done - "forward", "backward" or "central" for 3-point stencil and "" for 5-point stencil
        The lines to compute the differences in forward and backward differences are same, but the difference is in plotting them.
    """ 
    h=(x[-1]-x[0])/(n-1) #Step length
    
    if stencil==3: #Calculations using 3 point stencil
        
        if typ =="forward": #Forward difference
            df=np.zeros(len(f)-1)
            df=(f[1:]-f[:-1])/h
            plt.plot(x[:-1],df,label="first derivative")   
        
        elif typ == "backward":  #Backward difference
            df=np.zeros(len(f)-1)
            df=(f[1:]-f[:-1])/h
            plt.plot(x[1:],df,label="first derivative")
            
        elif typ == "central":  #Central difference
            df=np.zeros(len(f)-2)
            df=(f[2:]-f[:-2])/(2*h)    
            plt.plot(x[1:-1],df,label="first derivative")
    
    elif stencil==5:   #Calculations using 5 point stencil
        df=np.zeros(len(f)-4)      
        df=(-f[4:]+8*f[3:-1]-8*f[1:-3]+f[:-4])/(12*h)    
        plt.plot(x[2:-2],df,label="first derivative")
            

def second_order_diff (x,f,n,stencil):
    
    h=(x[-1]-x[0])/(n-1) #Calculating the value of each division
    
    if stencil==3:       #Calculations using 3 point stencil
        
        ddf=np.zeros(len(f)-2)     
        ddf=(f[2:]-2*f[1:-1]+f[:-2])/(h**2)   
        plt.plot(x[1:-1],ddf,label="second derivative")     
             
    elif stencil==5:    #Calculations using 5 point stencil
        
        ddf=np.zeros(len(f)-4)
        ddf=(-f[4:]+(16*f[3:-1])-(30*f[2:-2])+(16*f[1:-3])-f[:-4])/(12*(h**2))    
        plt.plot(x[2:-2],ddf,label="second derivative")    
    

n=15      #number of points to be considered     
x=np.linspace(0,1,n) #a linear space of x
f=x**2 #Define function here
plt.plot(x,f,label="function") #PLot the function
first_order_diff(x,f,n,3,"central") #call the function for first order differentiation
second_order_diff(x,f,n,3) #call the function for second order differentiation

plt.xlabel("x")
plt.ylabel("Result")
plt.legend(loc='best')
plt.show()
